class logTime{
    const sign = '_logTime_';


    function __construct() {
        $this->timeS = $this->currentTimeMillis();
        $this->timeE = 0;
        Yii::error('log time construct...');
    }
    function currentTimeMillis() {
        list($usec, $sec) = explode(" ", microtime());
        return round(((float)$usec + (float)$sec) * 1000);
    }

    function reset($tag = '')
    {
        $this->timeS = $this->currentTimeMillis();//microtime(true);
    }

    function log($tag)
    {
		echo $this->timeS;
		echo ', ' . $this->timeE . ' ';
        $this->timeE = $this->currentTimeMillis();//microtime(true);
        $str = sprintf(self::sign . ", %s >>> used time: [%d]------------------>>>",
					   $tag, $this->timeE - $this->timeS);
	    Yii::error($str);
    }
}